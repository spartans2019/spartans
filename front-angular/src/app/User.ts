export class User
{
    id_user:number;
    birthday:string;
    email:string;
    password:string;
    status:boolean;
    full_name:string;
    user_name:string;
    createdDate: string;
    editedDate: string;
}