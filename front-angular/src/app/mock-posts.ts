import { Post } from './post';

export const POSTS: Post[] = [
    {
        "idPost": 2,
        "idUser": 1,
        "text": "Este es el primer post",
        "createdDate": "2019-06-17T20:32:22.110+0000",
        "editedDate": '2019-06-17T20:32:22.110+0000'
    },
    {
        "idPost": 3,
        "idUser": 1,
        "text": "Este es el segundo post =D lolololoool",
        "createdDate": '1560805599999',
        "editedDate": '1560805599999'
    },
    {
        "idPost": 4,
        "idUser": 2,
        "text": "Jinguiry! Welp Tata!",
        "createdDate": "2019-06-17 20:33:19.999+0000",
        "editedDate": '2019-06-17 20:33:19.999+0000'
    },
    {
        "idPost": 6,
        "idUser": 2,
        "text": "=C :O pikachu face ^^",
        "createdDate": "2019-06-17T21:06:39.999+0000",
        "editedDate": "2019-06-17T21:06:39.999+0000"
    }
];