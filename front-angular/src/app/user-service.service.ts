import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './User';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService 
{
  private baseUrl: string;
  constructor(private http: HttpClient) 
  {
    this.baseUrl = 'http://localhost:8080/api/users';
    console.log(this.baseUrl);
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
 }
}
