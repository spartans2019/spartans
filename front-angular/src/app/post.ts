export class Post{
    idPost: number;
    idUser: number;
    text: string;
    createdDate: string;
    editedDate: string;
}
