import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/user-service.service';
import { User } from 'src/app/User';
import { RouterModule } from '@angular/router';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit
{
  users:User[];
  constructor(private userService: UserServiceService) { }

  ngOnInit() 
  {
    this.userService.findAll()
        .subscribe(data=>
          {
           this.users=data;
           console.log(this.users);
          });
  }

}
