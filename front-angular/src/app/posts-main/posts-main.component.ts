import { Component, OnInit } from '@angular/core';
import { Post } from "../post";
import { POSTS } from "../mock-posts";
import { PostsServiceService } from "../posts-service.service";

@Component({
  selector: 'app-posts-main',
  templateUrl: './posts-main.component.html',
  styleUrls: ['./posts-main.component.css']
})
export class PostsMainComponent implements OnInit {

  //posts = POSTS;
  posts: Post[];

  constructor(private postsServiceService: PostsServiceService) { }

  ngOnInit() {
    this.postsServiceService.findAll().subscribe(data => {
      this.posts = data;
    });
  }

}
