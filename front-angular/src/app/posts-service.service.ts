import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from './post';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class PostsServiceService {

  private usersUrl: string;
  private baseUrl: string;
  constructor(private http: HttpClient) {
    //this.usersUrl = 'http://10.0.75.1:8080/api/v1/posts';
    this.usersUrl = 'http://localhost:8080/api/v1/posts';
    
  }
 
  public findAll(): Observable<Post[]> {
    return this.http.get<Post[]>(this.usersUrl);
  }

}
