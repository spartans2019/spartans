package com.spartans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColpatriaDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColpatriaDemoApplication.class, args);
	}

}
