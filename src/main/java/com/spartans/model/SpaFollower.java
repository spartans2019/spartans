package com.spartans.model;

import javax.persistence.*;

@Entity
@Table(name = "spaLogs")
public class SpaFollower {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLog;
	private int idUserFollower;
	private int idUser;

	public int getIdLog() {
		return idLog;
	}

	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}

	public int getIdUserFollower() {
		return idUserFollower;
	}

	public void setIdUserFollower(int idUserFollower) {
		this.idUserFollower = idUserFollower;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
}
