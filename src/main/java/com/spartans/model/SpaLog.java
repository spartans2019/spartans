package com.spartans.model;

import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "spaLogs")
public class SpaLog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLog;
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp createdDate;
	private String text;
	private String trace;

	public int getIdLog() {
		return idLog;
	}

	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}
}
