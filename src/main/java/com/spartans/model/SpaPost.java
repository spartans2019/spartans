package com.spartans.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "spaPosts")
public class SpaPost {

	private int idPost;
    private int idUser;
    private String text;
    private Timestamp createdDate;
    private Timestamp editedDate;
 
    public SpaPost() {
    	
    }

	public SpaPost(int idPost, int idUser, String text, Timestamp createdDate, Timestamp editedDate) {
		this.idPost = idPost;
		this.idUser = idUser;
		this.text = text;
		this.createdDate = createdDate;
		this.editedDate = editedDate;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
		public int getIdPost() {
		return idPost;
	}
	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	@Column(name = "idUser", nullable = false)
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	@Column(name = "text", nullable = false)
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@Column(name = "createdDate", nullable = false)
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "editedDate", nullable = false)
	public Timestamp getEditedDate() {
		return editedDate;
	}
	public void setEditedDate(Timestamp editedDate) {
		this.editedDate = editedDate;
	}

	@Override
	public String toString() {
		return "SpaPost [idPost=" + idPost + ", idUser=" + idUser + ", text=" + text + ", createdDate=" + createdDate
				+ ", editedDate=" + editedDate + "]";
	}
	
}
