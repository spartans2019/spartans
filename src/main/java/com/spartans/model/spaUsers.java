package com.spartans.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="spaUser")
public class spaUsers 
{

	private long id_User;
	

	private String user_Name;
	private String _password;  
	private String full_Name;
	private String _email;
	private Date _birthday;
	private Boolean _status;
	private String created_Date;
	private String edited_Date;
	
	public spaUsers() {}
	
	public spaUsers(String user_Name, String _password, String full_Name, String _email, Date _birthday,
			Boolean _status, String created_Date, String edited_Date) 
	{
		this.user_Name = user_Name;
		this._password = _password;
		this.full_Name = full_Name;
		this._email = _email;
		this._birthday = _birthday;
		this._status = _status;
		this.created_Date = created_Date;
		this.edited_Date = edited_Date;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getId_User() {
		return id_User;
	}

	public void setId_User(long id_User) {
		this.id_User = id_User;
	}
	
	@Column(name="userName",nullable=false)
	public String getUser_Name() {
		return user_Name;
	}

	public void setUser_Name(String user_Name) {
		this.user_Name = user_Name;
	}
	@Column(name="password",nullable=false)
	public String get_password() {
		return _password;
	}

	public void set_password(String _password) {
		this._password = _password;
	}

	@Column(name="fullName",nullable=false)
	public String getFull_Name() {
		return full_Name;
	}

	public void setFull_Name(String full_Name) {
		this.full_Name = full_Name;
	}

	@Column(name="email",nullable=false)
	public String get_email() {
		return _email;
	}

	public void set_email(String _email) {
		this._email = _email;
	}

	@Column(name="birthday",nullable=false)
	public Date get_birthday() {
		return _birthday;
	}

	public void set_birthday(Date _birthday) {
		this._birthday = _birthday;
	}
	
	@Column(name="status",nullable=false)
	public Boolean get_status() {
		return _status;
	}

	public void set_status(Boolean _status) {
		this._status = _status;
	}

	@Column(name="CreatedDate",nullable=false)
	public String getCreated_Date() {
		return created_Date;
	}

	public void setCreated_Date(String created_Date) {
		this.created_Date = created_Date;
	}

	@Column(name="EditedDate",nullable=false)
	public String getEdited_Date() {
		return edited_Date;
	}

	public void setEdited_Date(String edited_Date) {
		this.edited_Date = edited_Date;
	}
	
}
