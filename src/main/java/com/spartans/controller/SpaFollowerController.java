package com.spartans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.spartans.exception.ResourceNotFoundException;
import com.spartans.model.SpaFollower;
import com.spartans.repository.FollowerRepository;

@RestController
@RequestMapping("/api/v1")
public class SpaFollowerController {
	@Autowired
	private FollowerRepository followerRepository;
	
	@GetMapping("/{id}/followers")
	public List<SpaFollower> getAllFollowers(@PathVariable(value = "id") Integer idUser){
		return followerRepository.findByIdUser(idUser);
	}
	
	@GetMapping("/followers/{id}")
    public ResponseEntity<SpaFollower> getPostById(@PathVariable(value = "id") Integer idFollower)
        throws ResourceNotFoundException {
    	SpaFollower spaFollower = followerRepository.findById(idFollower)
          .orElseThrow(() -> new ResourceNotFoundException("Follower not found for this id :: " + idFollower));
        return ResponseEntity.ok().body(spaFollower);
    }
	
	@PostMapping("/followers")
    public SpaFollower createFollower(@Valid @RequestBody SpaFollower spaFollower) {
        return followerRepository.save(spaFollower);
    }
	
	@DeleteMapping("/followers/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Integer idFollower)
         throws ResourceNotFoundException {
    	SpaFollower spaFollower = followerRepository.findById(idFollower)
       .orElseThrow(() -> new ResourceNotFoundException("Follower not found for this id :: " + idFollower));

    	followerRepository.delete(spaFollower);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
