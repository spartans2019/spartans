package com.spartans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spartans.exception.ResourceNotFoundException;
import com.spartans.model.SpaPost;
import com.spartans.repository.PostRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
public class SpaPostController {
	@Autowired
    private PostRepository postRepository;

    @GetMapping("/posts")
    public List<SpaPost> getAllPosts() {
        return postRepository.findAll();
    }
    
    @GetMapping("/posts/{id}")
    public ResponseEntity<SpaPost> getPostById(@PathVariable(value = "id") Integer idPost)
        throws ResourceNotFoundException {
    	SpaPost spaPost = postRepository.findById(idPost)
          .orElseThrow(() -> new ResourceNotFoundException("Post not found for this id :: " + idPost));
        return ResponseEntity.ok().body(spaPost);
    }
    
    @PostMapping("/posts")
    public SpaPost createPost(@Valid @RequestBody SpaPost spaPost) {
        return postRepository.save(spaPost);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<SpaPost> updatePost(@PathVariable(value = "id") Integer idPost,
         @Valid @RequestBody SpaPost postDetails) throws ResourceNotFoundException {
    	SpaPost spaPost = postRepository.findById(idPost)
        .orElseThrow(() -> new ResourceNotFoundException("Post not found for this id :: " + idPost));

    	spaPost.setIdUser(postDetails.getIdUser());
    	spaPost.setText(postDetails.getText());
    	spaPost.setEditedDate(postDetails.getEditedDate());
    	//The created date is not updatable
        final SpaPost updatedPost = postRepository.save(spaPost);
        return ResponseEntity.ok(updatedPost);
    }

    @DeleteMapping("/posts/{id}")
    public Map<String, Boolean> deletePost(@PathVariable(value = "id") Integer idPost)
         throws ResourceNotFoundException {
    	SpaPost spaPost = postRepository.findById(idPost)
       .orElseThrow(() -> new ResourceNotFoundException("Post not found for this id :: " + idPost));

        postRepository.delete(spaPost);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
}
