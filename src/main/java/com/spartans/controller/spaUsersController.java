package com.spartans.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spartans.exception.ResourceNotFoundException;

import com.spartans.model.spaUsers;
import com.spartans.repository.spaUsersRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
public class spaUsersController 
{
	@Autowired
	private spaUsersRepository _spaUserRepository;
	
	@GetMapping("/users")
	public List<spaUsers>GetAllUsers()
	{		
		return _spaUserRepository.findAll();
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<spaUsers>GetUserById(@PathVariable(value="id") Long userId)	
		throws ResourceNotFoundException
		{
		spaUsers user=_spaUserRepository.findById(userId)
					.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
			return ResponseEntity.ok().body(user);
		}
	
	@PostMapping("/users")
	public spaUsers CreateEmployee(@Valid @RequestBody spaUsers newUser)
	{
		return _spaUserRepository.save(newUser);
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<spaUsers>UpdateUser(@PathVariable(value="id") Long userId,@Valid
			@RequestBody spaUsers userDetails)
    throws ResourceNotFoundException
	{
		spaUsers user=_spaUserRepository.findById(userId)
				.orElseThrow(()->new ResourceNotFoundException("User not found for this id :: " + userId));
		
		user.setUser_Name(userDetails.getUser_Name());
		user.set_password(userDetails.get_password());		
		user.setFull_Name(userDetails.getFull_Name());
		user.set_email(userDetails.get_email());
		user.set_birthday(userDetails.get_birthday());
		user.set_status(userDetails.get_status());
		user.setCreated_Date(userDetails.getCreated_Date());
		user.setEdited_Date(userDetails.getEdited_Date());

		final spaUsers updateUser=_spaUserRepository.save(user);
		return ResponseEntity.ok(updateUser);
	}
	
	@DeleteMapping("/users/{id}")
	public Map<String,Boolean>DeleteUser(@PathVariable(value="id") Long userId)
	throws ResourceNotFoundException
	{
		spaUsers user=_spaUserRepository.findById(userId)
				.orElseThrow(()->new ResourceNotFoundException("User not found for this id :: " + userId));
		_spaUserRepository.delete(user);
		Map<String,Boolean>response=new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
		
	}
}
