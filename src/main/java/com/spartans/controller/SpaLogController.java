package com.spartans.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.spartans.exception.ResourceNotFoundException;
import com.spartans.model.SpaLog;
import com.spartans.repository.LogRepository;

@RestController
@RequestMapping("/api/v1")
public class SpaLogController {
	@Autowired
	private LogRepository logRepository;
	
	@GetMapping("/logs")
	public List<SpaLog> getAllLogs(){
		return logRepository.findAll();
	}
	
	@GetMapping("/logs/{id}")
    public ResponseEntity<SpaLog> getLogById(@PathVariable(value = "id") Integer idLog)
        throws ResourceNotFoundException {
    	SpaLog spaLog = logRepository.findById(idLog)
          .orElseThrow(() -> new ResourceNotFoundException("Log not found for this id :: " + idLog));
        return ResponseEntity.ok().body(spaLog);
    }
	
	@PostMapping("/logs")
    public SpaLog createLog(@Valid @RequestBody SpaLog spaLog) {
        return logRepository.save(spaLog);
    }
}
