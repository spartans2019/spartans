package com.spartans.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spartans.model.SpaLog;

@Repository
public interface LogRepository extends JpaRepository<SpaLog, Integer> {

}
