package com.spartans.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spartans.model.spaUsers;

@Repository
public interface spaUsersRepository extends JpaRepository<spaUsers,Long>
{

}
