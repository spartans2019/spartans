package com.spartans.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spartans.model.SpaFollower;

@Repository
public interface FollowerRepository extends JpaRepository<SpaFollower, Integer> {
	List<SpaFollower> findByIdUser(int IdUser);
}
